<?php
/**
 * @package active-plugins
 * @version 1.0
 *
 * Plugin Name: Active Plugins
 * Plugin URI: http://wordpress.org/extend/plugins/#
 * Description: This is a development plugin 
 * Author: Your Name
 * Version: 1.0
 * Author URI: https://example.com/
 */

add_shortcode( 'activeplugins', function(){
	$active_plugins = get_option( 'active_plugins' );
	$plugins = "";

	if( count( $active_plugins ) > 0 ){
		$plugins = "<ul>";

		foreach ( $active_plugins as $plugin ) {
			$plugins .= "<li>" . $plugin . "</li>";
		}

		$plugins .= "</ul>";
	}

	return $plugins;
});


$request_uri = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
$is_admin = strpos( $request_uri, '/wp-admin/' );

if( false === $is_admin ){
	add_filter( 'option_active_plugins', function( $plugins ){
		global $request_uri;
		$is_contact_page = strpos( $request_uri, '/kapcsolat/' );

		$myplugins = array( 
			//"contact-form-7/wp-contact-form-7.php"
		);

		if( false === $is_contact_page ){
			$plugins = array_diff( $plugins, $myplugins );
		}

		return $plugins;
	} );
}