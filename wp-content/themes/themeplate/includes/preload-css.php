<?php

function preloadCSS($base_filename, $media, $width) {
	$filepath = get_stylesheet_directory_uri() . '/assets/css/' . $base_filename . '.min.css';

	echo '<link rel="preload" href="' . $filepath . '" as="style" onload="this.onload=null;this.rel=\'stylesheet\'" media="' . ($media ? '(min-width: ' . $width . 'px)' : '' ) . '">
		<noscript><link rel="stylesheet" href="' . $filepath . '" media="' . ($media ? '(min-width: ' . $width . 'px)' : '' ) . '"></noscript>';
}
?>