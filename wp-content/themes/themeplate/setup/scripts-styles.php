<?php

/**
 * Enqueue scripts and styles
 *
 * @package ThemePlate
 * @since 0.1.0
 */

if ( ! function_exists( 'themeplate_scripts_styles' ) ) {
	function themeplate_scripts_styles() {
		$suffix = ( SCRIPT_DEBUG || THEME_DEBUG ) ? '' : '.min';

		// Deregister the jquery version bundled with WordPress
		// wp_deregister_script( 'jquery-core' );
		// wp_deregister_script( 'jquery-migrate' );
		// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header
		// wp_register_script( 'jquery-core', 'https://code.jquery.com/jquery-2.2.4' . $suffix . '.js', array(), '2.2.4', false );
		// wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-1.4.1' . $suffix . '.js', array(), '1.4.1', false );
		// wp_add_inline_script( 'jquery-core', 'jQuery.noConflict();' );

		// jQuery
		// wp_enqueue_script( 'jquery-core' );
		// Google Fonts
		// wp_enqueue_style( 'themeplate-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,700,900|Open+Sans:400,600,800', array(), THEME_VERSION );
		// Bootstrap
		// wp_enqueue_script( 'themeplate-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap' . $suffix . '.js', array(), '4.2.1', true );

		// Site scripts and styles
		// wp_enqueue_style( 'themeplate-style', THEME_URL . 'assets/css/themeplate' . $suffix . '.css', array(), THEME_VERSION );
		// wp_enqueue_style( 'themeplate-style', THEME_URL . 'assets/css/themeplate-tablet' . $suffix . '.css', array(), THEME_VERSION, '(min-width: 768px)' );
		// wp_enqueue_style( 'themeplate-style', THEME_URL . 'assets/css/themeplate-desktop' . $suffix . '.css', array(), THEME_VERSION, '(min-width: 1200px)' );
		wp_enqueue_script( 'themeplate-script', get_stylesheet_directory_uri() . '/assets/js/themeplate' . $suffix . '.js', array(), THEME_VERSION, true );

		$themeplate_options = array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script( 'themeplate-script', 'themeplate_options', apply_filters( 'themeplate_localize_script', $themeplate_options ) );
	}
	add_action( 'wp_enqueue_scripts', 'themeplate_scripts_styles', 20 );
}

// Async Scripts
if ( ! function_exists( 'themeplate_async_scripts' ) ) {
	function themeplate_async_scripts( $tag, $handle ) {
		// Add script handles
		$scripts = array();

		if ( in_array( $handle, $scripts, true ) ) {
			return str_replace( ' src', ' async="async" src', $tag );
		}

		return $tag;
	}
	add_filter( 'script_loader_tag', 'themeplate_async_scripts', 10, 2 );
}

// Defer Scripts
if ( ! function_exists( 'themeplate_defer_scripts' ) ) {
	function themeplate_defer_scripts( $tag, $handle ) {
		// Add script handles
		$scripts = array('themeplate-script');

		if ( in_array( $handle, $scripts, true ) ) {
			return str_replace( ' src', ' defer="defer" src', $tag );
		}

		return $tag;
	}
	add_filter( 'script_loader_tag', 'themeplate_defer_scripts', 10, 2 );
}

// Preload footer scripts (not to be used in parallel with async or defer)
// add_action('wp_head', function () {

//     global $wp_scripts;

//     foreach($wp_scripts->queue as $handle) {
//         $script = $wp_scripts->registered[$handle];

//         //-- Weird way to check if script is being enqueued in the footer.
//         if($script->extra['group'] === 1) {

//             //-- If version is set, append to end of source.
//             $source = $script->src . ($script->ver ? "?ver={$script->ver}" : "");

//             //-- Spit out the tag.
//             echo "<link rel='preload' href='{$source}' as='script'/>\n";
//         }
//     }
// }, 1);
