<?php

/**
 * Actions and Filters
 *
 * @package ThemePlate
 * @since 0.1.0
 */

add_action( 'after_switch_theme', 'flush_rewrite_rules' );

// Remove JPEG compression.
if ( ! function_exists( 'themeplate_jpeg_quality' ) ) {
	function themeplate_jpeg_quality() {
		return 100;
	}
	add_filter( 'jpeg_quality', 'themeplate_jpeg_quality' );
}

// Allow SVG upload
if ( ! function_exists( 'themeplate_mime_types' ) ) {
	function themeplate_mime_types( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	add_filter( 'upload_mimes', 'themeplate_mime_types' );
}

// Add SVG as image
if ( ! function_exists( 'themeplate_ext_types' ) ) {
	function themeplate_ext_types( $mimes ) {
		$mimes['image'][] = 'svg';
		return $mimes;
	}
	add_filter( 'ext2type', 'themeplate_ext_types' );
};

// Custom excerpt length
if ( ! function_exists( 'themeplate_excerpt_length' ) ) {
	function themeplate_excerpt_length( $length ) {
		return 50;
	}
	add_filter( 'excerpt_length', 'themeplate_excerpt_length' );
}

// Custom excerpt read more
if ( ! function_exists( 'themeplate_excerpt_string' ) ) {
	function themeplate_excerpt_string( $more ) {
		return '&hellip;';
	}
	add_filter( 'excerpt_more', 'themeplate_excerpt_string' );
}

// Number of revisions to keep
if ( ! function_exists( 'themeplate_keep_revisions' ) ) {
	function themeplate_keep_revisions( $num, $post ) {
		return 30;
	}
	add_filter( 'wp_revisions_to_keep', 'themeplate_keep_revisions', 10, 2 );
}

// Replace WP login screen logo.
if ( ! function_exists( 'themeplate_login_logo' ) ) {
	function themeplate_login_logo() {
		?>
		<style type="text/css">
			body.login h1 a {
				background-image: url( <?php echo esc_html( THEME_URL ); ?>screenshot.png );
				background-position: center;
				background-size: 440px 330px;
				width: 320px;
				height: 120px;
				box-shadow: 0 1px 3px rgba( 0, 0, 0, .13 );
			}
		</style>
		<?php
	}
	add_action( 'login_enqueue_scripts', 'themeplate_login_logo' );
};

// Link WP login logo to homepage.
if ( ! function_exists( 'themeplate_login_headerurl' ) ) {
	function themeplate_login_headerurl() {
		return home_url();
	}
	add_filter( 'login_headerurl', 'themeplate_login_headerurl' );
}

// Use the site title instead of 'WordPress'.
if ( ! function_exists( 'themeplate_login_headertitle' ) ) {
	function themeplate_login_headertitle() {
		return get_option( 'blogname' );
	}
	add_filter( 'login_headertitle', 'themeplate_login_headertitle' );
}
