<?php

/**
 * Template Name: Üzleti Partnereknek
 *
 * @package ThemePlate
 * @since 0.1.0
 */
?>

<?php get_header(); ?>
<header id="hero-business">
  <div class="blurred-bg"></div>
  <div class="container">
    <h1>ÜZLETI<br>PARTNEREINK<br>SZÁMÁRA</h1>
  </div>
  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/palyazat-logo.svg" alt="Széchényi 2020" data-no-lazy="1" class="palyazat-logo">
</header>

<div class="whitespace-100"></div>

<section id="business-services">
  <div class="container">
    <div class="align-center">
        <h2 class="section-title align-center less-margin-bottom">ÜZLETI SZOLGÁLTATÁSAINK</h2>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator-short">
    </div>
    <div class="row">
      <div class="col-lg-4 service">
        <?php picture('eszkoz-berles', 'jpeg', 'Vidék Újragondolva',  true, 'lazy'); ?>
        <h4 class="service-title">ESZKÖZ BÉRLÉS</h4>
        <p>VR és AR eszközeink kiválóan használhatóak céges csapatépítés alkalmával, vagy vállalati rendezvény különleges attrakciójaként.</p>
      </div>
      <div class="col-lg-4 service">
        <?php picture('eskuvo-helyszin', 'jpeg', 'Vidék Újragondolva',  true, 'lazy'); ?>
        <h4 class="service-title">ESKÜVŐ HELYSZÍN</h4>
        <p>Élményközpontunkat úgy alakítottuk ki, hogy az képes legyen esküvői násznépet és rendezvényt kiszolgálni. Termei átalakíthatóak és a VR attrakciók extra élményt tudnak hozzáadni a nagy nap eseményeihez. </p>
      </div>
      <div class="col-lg-4 service">
        <?php picture('konferencia-es-kepzes', 'jpeg', 'Vidék Újragondolva',  true, 'lazy'); ?>
        <h4 class="service-title">KONFERENCIA ÉS KÉPZÉS</h4>
        <p>Épületünk ideális konferenciák és szakmai továbbképzések helyszínének. Tágas előadótermei és koreszerű, erős számítógépekkel ellátott termei ideális körülményeket biztosítanak minden előadás számára.</p>
      </div>
    </div>
  </div>
</section>

<section id="rental-equipments">
  <div class="container">
    <div class="align-center extra-margin-bottom">
        <h2 class="section-title align-center less-margin-bottom">AZ ALÁBBI ESZKÖZÖK<br>BÉRLÉSÉRE BIZTOSÍTUNK LEHETŐSÉGET!</h2>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
    </div>
  </div>
  <div class="white-grey-bg">
      <div class="container">
          <div class="row extra-margin-top first-line">
            <div class="col-lg-6 img-box">
              <?php picture('htc-vive-cosmos', 'png', 'HTC Vive Cosmos',  true, 'lazy'); ?>
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small.svg" alt="Háromszög elem"  class="element-triangle-small">
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle top">
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle bottom">
            </div>
            <div class="col-lg-6 text-box">
              <h4 class="equipment-title">HTC VIVE COSMOS</h4>
              <p>Kiválóan használható céges csapatépítés alkalmával, vagy vállalati rendezvény különleges attrakciójaként.</p>
              <p class="rental-title">BÉRLÉSI DÍJ:</p>
              <p class="rental-price">100.000 Ft / nap + 90.000 Ft kaució</p>
              <div class="btn-holder">
                <a href="#" class="btn btn-primary btn-gradient-orange">TELEFONÁLOK<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div class="grey-white-bg">
      <div class="container">
          <div class="row extra-margin-top second-line">
            <div class="col-lg-6 text-box">
              <h4 class="equipment-title">HTC VIVE COSMOS</h4>
              <p>Kiválóan használható céges csapatépítés alkalmával, vagy vállalati rendezvény különleges attrakciójaként.</p>
              <p class="rental-title">BÉRLÉSI DÍJ:</p>
              <p class="rental-price">45.000 Ft / nap + 90.000 Ft kaució</p>
              <div class="btn-holder">
                <a href="#" class="btn btn-primary btn-gradient-orange">TELEFONÁLOK<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a>
              </div>
            </div>
            <div class="col-lg-6 img-box">
              <?php picture('okoshomokozo', 'png', 'Okoshomokozó',  true, 'lazy'); ?>
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small.svg" alt="Háromszög elem"  class="element-triangle-small">
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-reverse.svg" alt="Doboz elem"  class="element-box-reverse">
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-cilinder-mirror.svg" alt="Henger elem tükrözve"  class="element-cilinder-mirror">
            </div>
          </div>
        </div>
    </div>
</section>

<div class="whitespace-100"></div>

<section id="wedding-place">
    <div class="section-heading">
      <div class="container align-center">
        <h4 class="cat-title">ESKÜVŐHELYSZÍN</h4>
        <?php picture('wedding-place-img', 'jpeg', 'Esküvői Helyszín',  true, 'lazy'); ?>
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-reverse.svg" alt="Doboz elem"  class="element-box-reverse">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle top">
        <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle bottom">
      </div>
    </div>
    <div class="whitespace-100"></div>
    <div class="whitespace-100"></div>
    <div class="whitespace-100"></div>
    <div class="container align-center">
      <h2 class="section-title align-center less-margin-bottom">TARTSÁTOK NÁLUNK<br>AZ ESKÜVŐTŐKET!</h2>
      <p>Ünnepeljétek egy valóban különleges helyen a nagy napot! Tágas tereink és különleges helyszínünk mellett<br> lehetőségetek van mindenkinek a kedvében járni.<br>Szórakoztassátok a násznépet úgy, ahogy sehol máshol nem tudnátok.</p>
      <div class="btn-holder">
        <a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a>
    </div>
    <a href="#" class="btn-back"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/left-arrow.svg" alt="Jobbra nyíl"  class="left-arrow"></span>VISSZA</a>
    </div>
    <div class="whitespace-100"></div>
</section>


<?php get_footer(); ?>