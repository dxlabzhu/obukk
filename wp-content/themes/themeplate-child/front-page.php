<?php
/**
 * The main template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>
<header id="hero">
	<div class="container">
		<h1>AZ ÉLMÉNYEK<br>CSAK RÁD<br>VÁRNAK!</h1>
		<p>VR ÉS 3D ÉLMÉNYKÖZPONT<br>A VIDÉK SZÍVÉBEN!</p>
	</div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/palyazat-logo.svg" alt="Széchényi 2020" data-no-lazy="1" class="palyazat-logo">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/elements-bg.svg" alt="Háromszög és Négyszet hátul" data-no-lazy="1" class="elements-bg">
	<?php picture('htc_vive_osgyerek', 'png', 'Hero HTC Vive',  true, 'htc-vive'); ?>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/elements-fg.svg" alt="Háromszög és Négyszet elől" data-no-lazy="1" class="elements-fg">
</header>

<!-- Page Content -->
<div class="whitespace-100"></div>

<section id="country-refining">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 img-box">
				<?php picture('country-refining-img', 'jpeg', 'Vidék Újragondolva',  true, 'lazy'); ?>
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box.svg" alt="Doboz elem"  class="element-box">
			</div>
			<div class="col-lg-6 text-box">
				<h2 class="section-title">VIDÉK<br>ÚJRAGONDOLVA!</h2>
				<p class="section-desc">Óbükk Látogató- és Élményközpont ismerteti meg az érdeklődőkkel az itteni vidék különlegességeit a virtuális és kiterjesztett valóság segítségével. Ezen kívül számos lenyűgöző VR-élményt tartogat az érdeklődők számára.</p>
			</div>
		</div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
	<?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?>
</section>

<div class="whitespace-100"></div>

<section id="explore-the-caves">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text-box">
				<p class="cat-title">BARLANGTÚRA VIRTUÁLISAN</p>
				<h2 class="section-title">FEDEZD FEL<br>A BARLANGOK<br>TITKAIT</h2>
				<p class="section-desc">A környékünkön találhatóak veszélyes és különleges barlangok a mai napig tartogatnak titkokat. Fedezd fel ezeket a virtuális valóságon keresztül, biztonságos távolságból.</p>
				<div class="btn-holder">
					<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
				</div>
			</div>
			<div class="col-lg-6 img-box">
				<?php picture('explore-the-caves-img', 'jpeg', 'Barlangok Titkai',  true, 'lazy'); ?>
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small.svg" alt="Háromszög elem kicsi"  class="element-triangle-small">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-cilinder.svg" alt="Henger elem"  class="element-cilinder">
			</div>
		</div>
	</div>
	<?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?>
</section>

<section id="sky-tour">
	<div class="container">
		<div class="img-holder">
			<?php picture('sky-tour-img', 'jpeg', '360 fokos túra a város felett',  true, 'sky-tour-img'); ?>
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small.svg" alt="Háromszög elem kicsi"  class="element-triangle-small">
			<?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?>
		</div>	
		<p class="cat-title">VIRTUÁLIS FELHŐJÁRÁS</p>
    <h2 class="section-title">TEGYÉL 360° FOKOS TÚRÁT<br>500M MAGASBAN A VÁROS FELETT!</h2>
    <p class="section-desc">Ismerd meg ezt a lenyűgöző természeti tájat a magasból<br>korlátok és félelem nélkül. Lépkedj és teleportálj a felhők között.</p>
	<div class="btn-holder">
		<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
	</div>
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box.svg" alt="Doboz elem"  class="element-box">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
  </div>
  
</section>


<section id="treasure-of-bukk">
	<div class="container">
		<?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?>
		<div class="row">
			<div class="col-lg-6 img-box">
				<?php picture('treasure-of-bukk-img', 'jpeg', 'Óbükk kincse',  true, 'lazy'); ?>
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box.svg" alt="Doboz elem"  class="element-box">
			</div>
			<div class="col-lg-6 text-box">
				<p class="cat-title">DIGITÁLIS KINCSKERESŐ</p>
				<h2 class="section-title">TALÁLD MEG AZ<br> ÓBÜKK KINCSÉT</h2>
				<p class="section-desc">Az Óbükk térsége számtalan titkos kincset<br> rejteget.Találd meg őket a virtuális óbükk<br> erdőben ésszerezd meg a virtuális kincsvadászok elismerését.</p>
				<div class="btn-holder">
					<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
				</div>
			</div>
		</div>
		<div class="whitespace-100"></div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
</section>


<section id="smart-sandpit">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text-box">
				<p class="cat-title">OKOSHOMOKOZÓ</p>
				<h2 class="section-title">HOZZ LÉTRE<br>SAJÁT HEGYEKET<br>ÉS TAVAKAT!</h2>
				<p class="section-desc">Építs fel homokból saját hegyvonulatokat,<br>alakíts ki réteket, mélyítsd ki a tavakat.<br>Nem csak a képzeletedben lesz látható, hanem a valóságban is.<br>Az okoshomokozóban kialakítva mindez megjelenik bárki számára.</p>
				<div class="btn-holder">
					<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
				</div>
			</div>
			<div class="col-lg-6 img-box">
				<?php picture('smart-sandpit-img', 'jpeg', 'Okoshomokozó',  true, 'lazy'); ?>
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-flat.svg" alt="Doboz elem lapos"  class="element-box-flat">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-cilinder-reverse.svg" alt="Henger elem fordított"  class="element-cilinder-reverse">
			</div>
		</div>
		<div class="whitespace-100"></div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
</section>

<section id="walk-touchless">
	<div class="container">
		<div class="img-holder">
			<?php picture('walk-touchless-img', 'jpeg', 'Óbükk érintés nélkül',  true, 'lazy'); ?>
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box.svg" alt="Doboz elem"  class="element-box">
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small.svg" alt="Háromszög elem"  class="element-triangle-small">
		</div>
		<div class="align-center">
			<p class="cat-title">KÜLÖNVÉLEMÉNY–ÉLMÉNY</p>
			<h2 class="section-title align-center">JÁRD BE AZ ÓBÜKKÖT ÉRINTÉS NÉLKÜL</h2>
			<p class="section-desc">Nagyíts, pásztázz, és mozgasd a térképet amerre csak szeretnéd. Mindezt a levegőbe rajzolva, mint a filmekben.</p>
			<div class="btn-holder">
				<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
			</div>
		</div>
		<div class="whitespace-100"></div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
</section>

<section id="draw-things">
	<div class="container">
		<div class="img-holder">
			<?php picture('draw-things-img', 'jpeg', 'Rajzolj tárgyakat',  true, 'lazy'); ?>
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box.svg" alt="Doboz elem"  class="element-box">
		</div>
		<div class="align-center">
			<p class="cat-title">NYOMTASS 3D-BE A SAJÁT KEZEDDEL!</p>
			<h2 class="section-title align-center">RAJZOLVA HOZZ LÉTRE TÁRGYAKAT</h2>
			<p class="section-desc">Rajzolj a térbe és hozz létre elképesztő 3D-s alakzatokat a 3D-s ceruzánk segítségével</p>
			<div class="btn-holder">
				<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
			</div>
		</div>
		<div class="whitespace-100"></div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
</section>

<section id="print-in-3d">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 text-box">
				<p class="cat-title">3D SZKENNELÉS ÉS NYOMTATÁS</p>
				<h2 class="section-title">ÁLMODJ,<br>SZKENNELJ<br>ÉS NYOMTASS<br>3D-BEN</h2>
				<p class="section-desc">Képzeld el, hogy be tudod szkennelni<br>az egész fejedet, majd ki tudod nyomtatni<br>kulcstartónak. Nem vagány?<br>Nálunk lehetőséged van erre is.</p>
				<div class="btn-holder">
					<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a><a href="#" class="btn btn-primary btn-like">Like<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/like-icon.svg" alt="Like icon"  class="like-icon"></span></a>
				</div>
			</div>
			<div class="col-lg-6 img-box">
				<?php picture('print-in-3d-img', 'jpeg', '3d nyomtatás',  true, 'lazy'); ?>
			</div>
		</div>
		<div class="whitespace-100"></div>
		<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator">
	</div>
</section>

<div class="whitespace-100"></div>

<section id="more-excitement">
	<div class="container">
		<h2 class="section-title">TOVÁBBI<br>IZGALMAKRA VÁGYSZ?</h2>
		<p class="cat-title">ISMERD MEG VR LEHETŐSÉGEINKET!</p>
	</div>
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/curves.svg" alt="Görbe vonalak"  class="curves">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-reverse.svg" alt="Doboz elem"  class="element-box-reverse">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-flat.svg" alt="Doboz elem"  class="element-box-flat">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-cilinder-reverse.svg" alt="Henger elem"  class="element-cilinder-reverse">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle-small-reverse.svg" alt="Háromszög elem"  class="element-triangle-small-reverse">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle">
</section>

<div class="whitespace-100"></div>

<section id="vr-experiences">
  <div class="container">
    <div class="row">

      <?php 
        $args_vr = array(
          'post_type' => 'vr_experience',
          'tax_query' => array(
	        array(
	            'taxonomy' => 'category',
	            'field'    => 'slug',
	            'terms' => 'tovabbi_elmenyek',
	        	),
	    	),
          'posts_per_page' => 4,
          'orderby' => 'post_date',
          'order' => 'DESC',
        );

        $loop_vr = new WP_Query( $args_vr );
      ?>
      <?php if ( $loop_vr->have_posts() ) : ?>
        <?php while ( $loop_vr->have_posts() ) : ?>
          <?php $loop_vr->the_post(); ?>

          <div class="col-lg-6 mb-6">
            <div class="card h-100">
              <div class="card-img">
              <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
              <?php } else { ?>
                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
              <?php } ?>
              </div>
              
              <div class="card-body">
              	<a href="<?php the_permalink(); ?>">
              	 <h5 class="post-title"><?php the_title(); ?></h5>
                  <div class="entry-content">
                    <?php the_excerpt(); ?>
                  </div>
                  </a>
                </div>
              </div>
            </div><!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <!-- /.row -->
  </div>
</section>

<div class="whitespace-100"></div>

<section id="offers-for-schools">
	<div class="section-heading">
		<div class="container">
			<h2 class="section-title">AJÁNLATAINK ISKOLÁS<br>CSOPORTOK SZÁMÁRA</h2>
			<p class="cat-title">EMELJÜK EGY ÚJ SZINTRE AZ ISKOLA ÓRÁK TARTALMÁT</p>
			<?php picture('schoolboy', 'png', 'Iskolás gyerek',  true, 'schoolboy'); ?>
		</div>
	</div>
	<div class="section-content">
	  <div class="container">
	    <div class="row">

	      <?php 
	        $args_offers = array(
	          'post_type' => 'offers-for-schools',
	          'posts_per_page' => 5,
	          'orderby' => 'post_date',
	          'order' => 'DESC',
	        );

	        $loop_offers = new WP_Query( $args_offers );
	        $post_counter = 0;
	      ?>
	      <?php if ( $loop_offers->have_posts() ) : ?>
	        <?php while ( $loop_offers->have_posts() ) : ?>
	          <?php $loop_offers->the_post(); ?>
	          <?php $post_counter++ ?>
	          <div class="col-lg-12 mb-12">
	          	<?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?>
	            <div class="card h-100 row">
	             <?php if ($post_counter == 2) { ?>
	             	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-cilinder-reverse.svg" alt="Henger elem fordított"  class="element-cilinder-reverse">
	             <?php } elseif ($post_counter == 3) { ?>
	            	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-flat.svg" alt="Doboz elem lapos"  class="element-box-flat">
	             <?php  } elseif ($post_counter == 4) { ?>
	               	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle">
	             <?php  } ?>
	              <div class="card-img col-lg-4">
	              <?php if ( has_post_thumbnail() ) { ?>
	                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
	              <?php } else { ?>
	                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
	              <?php } ?>
	              </div>
	              
	              <div class="card-body col-lg-8">
	              	 <h5 class="post-title"><?php the_title(); ?></h5>
	              	 <p class="number-of-participant"><?php echo get_field('hany_fo');?> fő részére egyidőben</p>
	                  <div class="entry-content">
	                    <?php the_excerpt(); ?>
	                  </div>
		  				<div class="btn-holder">
							<a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a>
						</div>
	                </div>
	              </div>
	            </div><!-- #post-<?php the_ID(); ?> -->

	          <?php endwhile; ?>
	        <?php endif; ?>
	        <?php wp_reset_postdata(); ?>
	      </div>
	      <!-- /.row -->
	  </div>
	</div>
	<div class="section-footer">
		
	</div>
</section>



<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h2 class="section-title">KAPCSOLAT</h2>
				<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
			</div>
			<div class="col-lg-6 img-box">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-triangle.svg" alt="Háromszög elem"  class="element-triangle">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-flat.svg" alt="Doboz elem lapos"  class="element-box-flat">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/element-box-reverse.svg" alt="Doboz elem"  class="element-box-reverse">
				<?php picture('contact-img', 'jpeg', 'Kapcsolat',  true, 'lazy'); ?>
			</div>
		</div>

	</div>
</section>

  <?php

  get_footer();
