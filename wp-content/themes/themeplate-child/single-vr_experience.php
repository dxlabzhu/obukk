<?php

/**
 * The template for displaying single posts
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<section id="single-vr-experience-post">

	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<header style="background-image: url('<?php echo $thumb['0'];?>')">
	  <div class="blurred-bg"><?php picture('colored-line', 'png', 'Színes vonal',  true, 'lazy colored-line'); ?></div>
	  <div class="container">
	    <h1><?php the_title(); ?></h1>
	  </div>
	  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/palyazat-logo.svg" alt="Széchényi 2020" data-no-lazy="1" class="palyazat-logo">
	</header>

	<div class="whitespace-100"></div>
	<div class="whitespace-100"></div>

	<div class="container">
		
			<main class="content">

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="row">
				<div class="col-lg-5 text-box">
					<p class="age-group"><?php echo get_field('ajanlott_korosztaly_tol') . '-' . get_field('ajanlott_korosztaly_ig') . ' éves korig';?>
					<h2>FELADAT</h2>
					<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator-short">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
					<ul class="params">
						<li class="mode">A feladat <?php echo get_field('mod');?> módban játszható</li>
						<li class="mode">A feladatra <?php echo get_field('rendelkezesre_allo_ido');?> perc áll rendelkezésre</li>
					</ul>
					<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/grey-dot-separator.svg" alt="Elválasztó vonal" class="grey-dot-separator-short">
				</div>
				<?php $image = get_field('bemutatokep'); ?>
				<div class="col-lg-6 img-box" style="background-image: url('<?php echo esc_url($image['url']); ?>')">
					<?php picture('img-mask', 'png', 'Kép maszk',  true, 'lazy'); ?>
					<p class="price"><?php echo get_field('ar') . ' Ft / fő / '. get_field('rendelkezesre_allo_ido') .' perc';?></p>
				</div>
			</div>
			</article>
				<?php endwhile; ?>
			</main>
		<div class="grey-box">
			<div class="row">
				<div class="col-lg-7">
					<p>Ne csak olvasgass, foglalj időpontot és éld át ezt a különleges élményt minél hamarabb!</p>
				</div>
				<div class="col-lg-5">
	              <div class="btn-holder">
	                <a href="#" class="btn btn-primary btn-gradient-orange">IDŐPONTFOGLALÁS MOST!<span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/right-arrow.svg" alt="Jobbra nyíl"  class="right-arrow"></span></a>
	              </div>
				</div>
			</div>
		</div>
		<div class="whitespace-100"></div>
		   <a href="#" class="btn-back"><span><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/left-arrow.svg" alt="Jobbra nyíl"  class="left-arrow"></span>VISSZA</a>
	   <div class="whitespace-100"></div>
	</div>
</section>
<?php

get_footer();
